from django.db import models
from django.contrib.auth.models import User
from django.conf import settings


class TinyUrl(models.Model):

    BLACKLIST = ['PIZZA', 'CUKIER', ]
    # list of words that should not be used as tiny URLs
    # in the real life app this list would contain offensive words

    original_url = models.TextField()
    tiny_url = models.CharField(max_length=settings.SHORT_URL_LENGTH_BOUNDS[1])
    visit_counter = models.IntegerField()
    author = models.ForeignKey(User, models.CASCADE,
                               related_name='tiny_url',)
