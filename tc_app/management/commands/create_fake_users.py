from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
import requests
import json


class Command(BaseCommand):
    help = 'Create random users'

    def add_arguments(self, parser):
        parser.add_argument('total', type=int, help='Indicates the number of users to be created')

    def handle(self, *args, **kwargs):
        total = kwargs['total']
        url = 'https://randomuser.me/api/?results=%s' % total
        r = requests.get(url)
        user_data = json.loads(r.text)
        for i in range(total):
            User.objects.create_user(username=user_data['results'][i]['login']['username'],
                                     last_name=user_data['results'][i]['name']['last'],
                                     email=user_data['results'][i]['email'],
                                     password=user_data['results'][i]['login']['password'],
                                     date_joined=user_data['results'][i]['registered']['date'],
                                     )
