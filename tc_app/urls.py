from django.urls import path


from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('!<str:tiny_url>', views.short_info, name='short_info'),
    path('<str:tiny_url>', views.short_redirect, name='short_redirect'),
]
