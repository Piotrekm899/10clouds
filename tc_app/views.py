from django.shortcuts import render, redirect, get_object_or_404
from django.conf import settings
from django.db.models.aggregates import Count


import django.utils.crypto as crypto
from .forms import TinyForm
from . import models
import random


def index(request):
    if request.method == 'POST':
        form = TinyForm(request.POST)
        if form.is_valid():
            input_url = request.POST.get('url')
            try:
                tiny_object = models.TinyUrl.objects.get(original_url=input_url)
            except models.TinyUrl.DoesNotExist:
                length = random.randint(settings.SHORT_URL_LENGTH_BOUNDS[0], settings.SHORT_URL_LENGTH_BOUNDS[1])
                tiny_url = crypto.get_random_string(length=length).upper()
                while models.TinyUrl.objects.filter(tiny_url=tiny_url).exists() or tiny_url in models.TinyUrl.BLACKLIST:
                    tiny_url = crypto.get_random_string(length=length).upper()
                tiny_object = models.TinyUrl.objects.create(original_url=input_url,
                                                            tiny_url=tiny_url,
                                                            visit_counter=0,
                                                            author=get_random_user()
                                                            )
            return render(request, 'short.html', {'tiny_object': tiny_object})
    else:
        form = TinyForm()
    return render(request, 'index.html', {'form': form})


def get_random_user():
    count = models.User.objects.aggregate(count=Count('id'))['count']
    random_index = random.randint(1, count - 1)
    return models.User.objects.all()[random_index]


def short_redirect(request, tiny_url):
    tiny_object = get_object_or_404(models.TinyUrl, tiny_url=tiny_url)
    tiny_object.visit_counter += 1
    tiny_object.save()
    return redirect(tiny_object.original_url)


def short_info(request, tiny_url):
    tiny_object = get_object_or_404(models.TinyUrl, tiny_url=tiny_url)
    return render(request, 'short_info.html', {'tiny_object': tiny_object})

