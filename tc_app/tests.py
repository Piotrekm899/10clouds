from django.forms import ValidationError
from django.core.management import call_command
from django.core.validators import URLValidator
from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse
from http import HTTPStatus
from tc_app import models
from tc_app.views import get_random_user
from tc_app.forms import TinyForm


class CommandsTestCase(TestCase):

    def test_create_fake_users(self):
        args = [20]
        opts = {}
        call_command('create_fake_users', *args, **opts)

        self.assertEqual(len(User.objects.all()), 20)


class TinyUrlTest(TestCase):

    def test_urls(self):
        url = reverse('short_info', args=['AEIOU'])
        self.assertEqual(url, '/!AEIOU')

        url = reverse('short_redirect', args=['AEIOU'])
        self.assertEqual(url, '/AEIOU')

    def test_get_random_user(self):
        args = [20]
        opts = {}
        call_command('create_fake_users', *args, **opts)
        random_user = get_random_user()
        self.assertIn(random_user, models.User.objects.all())

    def test_short_redirect_and_info(self):
        args = [2]
        opts = {}
        call_command('create_fake_users', *args, **opts)
        tiny_object = models.TinyUrl.objects.create(original_url='https://test.com',
                                                    tiny_url='AEIOU',
                                                    visit_counter=0,
                                                    author=get_random_user()
                                                    )
        url = reverse('short_redirect', kwargs={'tiny_url': 'AEIOU'})
        response = self.client.get(url)
        self.assertRedirects(response, 'https://test.com', status_code=HTTPStatus.FOUND, fetch_redirect_response=False)
        tiny_object.refresh_from_db()
        self.assertEqual(tiny_object.visit_counter, 1)
        self.client.get(url)
        tiny_object.refresh_from_db()
        self.assertEqual(tiny_object.visit_counter, 2)

        url = reverse('short_info', kwargs={'tiny_url': 'AEIOU'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_invalid_form(self):
        form_data = {'url': 'test.com'}
        tiny_form = TinyForm(data=form_data)
        self.assertFalse(tiny_form.is_valid())

        url = reverse('index')
        self.client.post(url, {'url': form_data['url']})
        val = URLValidator()
        self.assertRaisesRegexp(ValidationError, 'Enter a valid URL.', val, form_data['url'])

    def test_no_form_data(self):
        url = reverse('index')
        response = self.client.post(url)
        self.assertFormError(response, 'form', 'url', 'This field is required.')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_valid_form(self):

        form_data = {'url': 'https://test.com'}
        tiny_form = TinyForm(data=form_data)
        self.assertTrue(tiny_form.is_valid())

        args = [2]
        opts = {}
        call_command('create_fake_users', *args, **opts)

        url = reverse('index')
        response = self.client.post(url, {'url': form_data['url']})
        self.assertEqual(response.status_code, HTTPStatus.OK)

        tiny_object = models.TinyUrl.objects.get(original_url=form_data['url'])

        self.assertEqual(tiny_object.visit_counter, 0)
        self.assertEqual(tiny_object.original_url, form_data['url'])
        self.assertIn(tiny_object.author, models.User.objects.all())

    def test_same_url_twice(self):
        args = [2]
        opts = {}
        call_command('create_fake_users', *args, **opts)

        form_data = {'url': 'https://test.com'}
        url = reverse('index')
        response = self.client.post(url, {'url': form_data['url']})
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(len(models.TinyUrl.objects.all()), 1)

        response = self.client.post(url, {'url': form_data['url']})
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(len(models.TinyUrl.objects.all()), 1)





