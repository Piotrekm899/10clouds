from django import forms
from django.core.validators import URLValidator


class TinyForm(forms.Form):
    url = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'http://example.com'}))

    def clean(self):
        cleaned_data = super(TinyForm, self).clean()
        url = cleaned_data.get('url')

        if url:
            validate = URLValidator()
            validate(url)
        else:
            raise forms.ValidationError('Please provide your url')

